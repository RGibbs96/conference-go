from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city} {state}&per_page=1"
    response = requests.get(url, headers=headers)
    photo_info = json.loads(response.content)
    try:
        return photo_info["photos"][0]["src"]["original"]
    except:
        return {"": None}


def get_current_weather(city_name):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city_name}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    try:
        response = requests.get(url)
        location_info = json.loads(response.content)
        lat = location_info[0]["lat"]
        lon = location_info[0]["lon"]
    except:
        return {"weather": None}
    # with lat and lon info, get weather
    try:
        url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
        weather_response = requests.get(url)
        weather_info = json.loads(weather_response.content)
        temp_K = weather_info["main"]["temp"]
        temp_F = (temp_K - 273.15) * (9 / 5) + 32
        weather_description = weather_info["weather"][0]["description"]
        return {"temp": temp_F, "description": weather_description}
    except:
        return {"weather": None}
