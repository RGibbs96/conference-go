import json
import requests

from .models import ConferenceVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"  # defining the URL associated with all conferences in the monolith
    response = requests.get(
        url
    )  # make a get request to the previous url, storing response
    content = json.loads(
        response.content
    )  # load the json content into a python readable structure
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
